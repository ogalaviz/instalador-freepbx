#!/bin/bash
URL_FREEPBX=http://mirror.freepbx.org/modules/packages/freepbx/freepbx-13.0-latest.tgz
#URL_ASTERISK=http://downloads.asterisk.org/pub/telephony/certified-asterisk/asterisk-certified-13.18-current.tar.gz
URL_ASTERISK=http://downloads.asterisk.org/pub/telephony/certified-asterisk/asterisk-certified-13.21-current.tar.gz
URL_DIALPLAN=https://gitlab.com/kiwanoplus/instalador-freepbx/raw/master/extensions_kiwano.conf
URL_FPBXCONFIG=https://gitlab.com/kiwanoplus/instalador-freepbx/raw/master/fwconsole.settings
URL_FPBXCSS=https://gitlab.com/kiwanoplus/instalador-freepbx/raw/master/kiwano.css
URL_FPBXLOGO=https://gitlab.com/kiwanoplus/instalador-freepbx/raw/master/logo.png
URL_WEBMIN=http://download.webmin.com/download/yum
URL_WEBMINKEY=http://www.webmin.com/jcameron-key.asc
URL_LOWAYREPO=http://yum.loway.ch/loway.repo
URL_AUDIOFILES=https://174.138.40.219:8443/ivrcenter2/files/voces_espanol.tar.gz
URL_G729INTEL=http://asterisk.hosting.lv/bin/codec_g729-ast130-gcc4-glibc-x86_64-core2-sse4.so
URL_G729AMD=http://asterisk.hosting.lv/bin/codec_g729-ast130-gcc4-glibc-x86_64-opteron-sse3.so
URL_MYIP=http://icanhazip.com

# Declaracion de Funciones

# Prepara el entorno de instalacion, dependencias y variables
SetupEnv() {
    sed -i 's/#Port 22/Port 58790/g' /etc/ssh/sshd_config
    systemctl restart sshd
    sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config
    setenforce permissive
    rm -f /etc/localtime && ln -s /usr/share/zoneinfo/America/Bogota /etc/localtime
    cat >/etc/yum.repos.d/webmin.repo <<EOF
[Webmin]
name=Webmin Distribution Neutral
#baseurl=$URL_WEBMIN
mirrorlist=$URL_WEBMIN/mirrorlist
enabled=1
EOF
    rpm --import $URL_WEBMINKEY
    curl -Lo /etc/yum.repos.d/loway.repo $URL_LOWAYREPO
    yum -y install\
      epel-release
    yum -y install\
      cronie\
      fail2ban\
      gcc-c++\
      {gsm,jansson,libcurl,libsrtp,libtool-ltdl,libuuid,libxml2,ncurses,openssl,speex,sqlite,unixODBC}-devel\
      make\
      {mariadb,tftp}-server\
      mysql-connector-odbc\
      {net,sysvinit}-tools\
      nfs-utils\
      ntp\
      nginx\
      openvpn\
      php-{cli,fpm,mbstring,mysql,pear}\
      psmisc\
      sox\
      sudo\
      tcpdump\
      vim\
      webmin
    curl -Lo /usr/local/src/asterisk-certified.tgz $URL_ASTERISK
    curl -Lo /usr/local/src/freepbx-13.tgz $URL_FREEPBX
    tar zxf /usr/local/src/asterisk-certified.tgz -C /usr/local/src && rm -f /usr/local/src/asterisk-certified.tgz
    tar zxf /usr/local/src/freepbx-13.tgz -C /usr/local/src && rm -f /usr/local/src/freepbx-13.tgz
}

# Prepara la base de datos MariaDB
SetupMariaDB() {
    mysql_install_db
    chown -R mysql. /var/lib/mysql
    systemctl enable mariadb
    systemctl stop mariadb
    systemctl start mariadb
}

# Instala Asterisk desde las fuentes especificadas
InstallAsterisk() {
    useradd -rd /var/lib/asterisk asterisk
    cd /usr/local/src/asterisk-certified-13.*
    ./configure --libdir=/usr/lib64
    make menuselect.makeopts
    menuselect/menuselect \
      --disable BUILD_NATIVE \
      menuselect.makeopts
    make
    make install
    make config
    ldconfig
    chown asterisk. /var/run/asterisk
    chown -R asterisk. /etc/asterisk
    chown -R asterisk. /var/{lib,log,spool}/asterisk
    chown -R asterisk. /usr/lib64/asterisk
    systemctl stop asterisk
    systemctl disable asterisk
}

# Instala los archivos de audio en español
InstallAudioFiles() {
    curl -Lo /var/lib/asterisk/sounds/voces_espanol.tgz $URL_AUDIOFILES
    tar zxf /var/lib/asterisk/sounds/voces_espanol.tgz -C /var/lib/asterisk/sounds && rm -f /var/lib/asterisk/sounds/voces_espanol.tgz
}

# Instala el codec g729 de implementación abierta
InstallG729() {
    vendor="$(grep vendor_id /proc/cpuinfo | head -1 | awk '{print $3}')"
    case "$vendor" in
        AuthenticAMD)
            curl -Lo /usr/lib64/asterisk/modules/codec_g729.so $URL_G729INTEL
            ;;
        GenuineIntel)
            curl -Lo /usr/lib64/asterisk/modules/codec_g729.so $URL_G729AMD
            ;;
    esac
    chown asterisk. /usr/lib64/asterisk/modules/codec_g729.so
    chmod +x /usr/lib64/asterisk/modules/codec_g729.so
}

# Instala FreePBX desde la ubicacion especificada
InstallFreePBX() {
    mkdir /usr/share/nginx/freepbx
    mkdir -p /var/lib/php/session
    chown asterisk. /var/lib/php/session
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
      -keyout /etc/pki/tls/certs/server.key -out /etc/pki/tls/certs/server.crt \
      -subj "/C=US/ST=FL/L=Miami/O=Kiwano Plus Tech/OU=IT Department/CN=localhost"
    cat >/etc/nginx/conf.d/freepbx.conf <<EOL
server {
    listen 8443 ssl default_server;
    listen [::]:8443 ssl default_server;
    root /usr/share/nginx/freepbx;
    client_max_body_size 8M;
    index index.php index.html index.htm;
    server_name ippbx_kiwano;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers ECDHE+RSAGCM:ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:!aNULL!eNull:!EXPORT:!DES:!3DES:!MD5:!DSS;
    ssl_certificate      /etc/pki/tls/certs/server.crt;
    ssl_certificate_key  /etc/pki/tls/certs/server.key;
    location / {
        try_files \$uri \$uri/ /index.php;
    }
    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
        fastcgi_buffer_size 128k;
        fastcgi_buffers 256 4k;
        fastcgi_busy_buffers_size 256k;
        fastcgi_temp_file_write_size 256k;
        fastcgi_read_timeout 300;
    }
}
EOL
    cat >/etc/php-fpm.d/www.conf <<EOL
[www]
listen = /var/run/php-fpm/php-fpm.sock
listen.allowed_clients = 127.0.0.1
listen.owner = nobody
listen.group = nobody
user = asterisk
group = asterisk
pm = dynamic
pm.max_children = 50
pm.start_servers = 2
pm.min_spare_servers = 2
pm.max_spare_servers = 5
slowlog = /var/log/php-fpm/www-slow.log
request_terminate_timeout = 300
php_admin_value[error_log] = /var/log/php-fpm/www-error.log
php_admin_flag[log_errors] = on
php_value[session.save_handler] = files
php_value[session.save_path] = /var/lib/php/session
EOL
    cat >/etc/php.ini <<EOL
[PHP]
engine = On
short_open_tag = Off
asp_tags = Off
precision = 14
output_buffering = 4096
zlib.output_compression = Off
implicit_flush = Off
unserialize_callback_func =
serialize_precision = 17
disable_functions =
disable_classes =
zend.enable_gc = On
expose_php = On
max_execution_time = 300
max_input_time = 60
memory_limit = 256M
error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT
display_errors = Off
display_startup_errors = Off
log_errors = On
log_errors_max_len = 1024
ignore_repeated_errors = Off
ignore_repeated_source = Off
report_memleaks = On
track_errors = Off
html_errors = On
variables_order = "GPCS"
request_order = "GP"
register_argc_argv = Off
auto_globals_jit = On
post_max_size = 8M
auto_prepend_file =
auto_append_file =
default_mimetype = "text/html"
doc_root =
user_dir =
enable_dl = Off
cgi.fix_pathinfo=0
file_uploads = On
upload_max_filesize = 8M
max_file_uploads = 20
allow_url_fopen = On
allow_url_include = Off
default_socket_timeout = 60
[CLI Server]
cli_server.color = On
[Pdo_mysql]
pdo_mysql.cache_size = 2000
pdo_mysql.default_socket=
[mail function]
SMTP = localhost
smtp_port = 25
sendmail_path = /usr/sbin/sendmail -t -i
mail.add_x_header = On
[SQL]
sql.safe_mode = Off
[ODBC]
odbc.allow_persistent = On
odbc.check_persistent = On
odbc.max_persistent = -1
odbc.max_links = -1
odbc.defaultlrl = 4096
odbc.defaultbinmode = 1
[Interbase]
ibase.allow_persistent = 1
ibase.max_persistent = -1
ibase.max_links = -1
ibase.timestampformat = "%Y-%m-%d %H:%M:%S"
ibase.dateformat = "%Y-%m-%d"
ibase.timeformat = "%H:%M:%S"
[MySQLi]
mysqli.max_persistent = -1
mysqli.allow_persistent = On
mysqli.max_links = -1
mysqli.cache_size = 2000
mysqli.reconnect = Off
[mysqlnd]
mysqlnd.collect_statistics = On
mysqlnd.collect_memory_statistics = Off
[Session]
session.save_handler = files
session.use_cookies = 1
session.use_only_cookies = 1
session.name = PHPSESSID
session.auto_start = 0
session.cookie_lifetime = 0
session.cookie_path = /
session.cookie_domain =
session.cookie_httponly =
session.serialize_handler = php
session.gc_probability = 1
session.gc_divisor = 1000
session.gc_maxlifetime = 1440
session.bug_compat_42 = Off
session.bug_compat_warn = Off
session.referer_check =
session.cache_limiter = nocache
session.cache_expire = 180
session.use_trans_sid = 0
session.hash_function = 0
session.hash_bits_per_character = 5
url_rewriter.tags = "a=href,area=href,frame=src,input=src,form=fakeentry"
[Tidy]
tidy.clean_output = Off
[soap]
soap.wsdl_cache_enabled=1
soap.wsdl_cache_dir="/tmp"
soap.wsdl_cache_ttl=86400
soap.wsdl_cache_limit = 5
[ldap]
ldap.max_links = -1
EOL
    FPBXPWD=`uuidgen | md5sum | awk '{print $1}'`
    mysql -u root <<EOF
CREATE DATABASE asterisk;
CREATE DATABASE asteriskcdrdb;
#GRANT ALL PRIVILEGES ON asterisk.* to 'freepbxuser'@'localhost' IDENTIFIED BY "$FPBXPWD";
GRANT ALL PRIVILEGES ON asterisk.* to 'freepbxuser'@'localhost' IDENTIFIED BY "aSd123qWe456";
#GRANT ALL PRIVILEGES ON asteriskcdrdb.* to 'freepbxuser'@'localhost' IDENTIFIED BY "$FPBXPWD";
GRANT ALL PRIVILEGES ON asteriskcdrdb.* to 'freepbxuser'@'localhost' IDENTIFIED BY "aSd123qWe456";
FLUSH PRIVILEGES;
EOF
    (cd /usr/local/src/freepbx &&\
      ./start_asterisk start)
    (cd /usr/local/src/freepbx &&\
#      ./install --webroot /usr/share/nginx/freepbx --dbuser freepbxuser --dbpass $FPBXPWD --no-interaction)
      ./install --webroot /usr/share/nginx/freepbx --dbuser freepbxuser --dbpass aSd123qWe456 --no-interaction)
    fwconsole util signaturecheck
    fwconsole reload
    fwconsole stop
    systemctl enable php-fpm
    systemctl enable nginx
    systemctl start php-fpm
    systemctl start nginx
    cat >/etc/systemd/system/freepbx.service <<EOF
[Unit]
Description=FreePBX VoIP Server
After=mariadb.service
[Service]
Type=oneshot
RemainAfterExit=yes
Environment="TERM=xterm-256color"
ExecStart=/usr/sbin/fwconsole start
ExecReload=/usr/sbin/fwconsole reload
ExecStop=/usr/sbin/fwconsole stop
[Install]
WantedBy=multi-user.target
EOF
    systemctl daemon-reload
    systemctl enable freepbx
    systemctl start freepbx
}

SetupFreePBX() {
    MYIP=`curl -L $URL_MYIP`
    MYHOSTNAME=`hostname -s`
    curl -Lo /etc/asterisk/extensions_kiwano.conf $URL_DIALPLAN
    curl -Lo /dev/shm/fwconsole.settings $URL_FPBXCONFIG
    curl -Lo /usr/share/nginx/freepbx/admin/assets/css/kiwano.css $URL_FPBXCSS
    curl -Lo /usr/share/nginx/freepbx/admin/images/logo.png $URL_FPBXLOGO
    cat >/etc/asterisk/freepbx_chown.conf <<EOF
[blacklist]
directory = /var/spool/asterisk/monitor/
directory = /var/spool/asterisk/qm_monitor/
EOF
    echo "#include extensions_kiwano.conf" >> /etc/asterisk/extensions_custom.conf
    echo "KIWANO_EXTEN_LENGTH = 3" >> /etc/asterisk/globals_custom.conf
    fwconsole ma enablerepo standard
    fwconsole ma enablerepo extended
    fwconsole ma enablerepo unsupported
    fwconsole ma downloadinstall announcement backup bulkhandler endpointman findmefollow ivr manager miscdests outroutemsg pinsets queues ringgroups timeconditions
    fwconsole ma downloadinstall customcontexts queuemetrics
    mysql -u root asterisk <<EOF
DELETE FROM trunks;
INSERT
  INTO logfile_logfiles (name,error,notice,warning)
  VALUES ('messages','on','on','on');
UPDATE queuemetrics_options
  SET value='true'
  WHERE keyword='ivr_logging';
UPDATE sipsettings
  SET data='6060'
  WHERE keyword='bindport';
INSERT
  INTO ampusers (username,password_sha1,sections)
  VALUES ('technicalsupport','557d2cdf3cba4a2d1fc1263a604c5a2d664c7ba2','*');
INSERT
  INTO outroutemsg (keyword,data)
  VALUES
    ('default_msg_id','-2'),
    ('emergency_msg_id','-2'),
    ('intracompany_msg_id','-2'),
    ('invalidnmbr_msg_id','-2'),
    ('no_answer_msg_id','-2')
  ON DUPLICATE KEY UPDATE data=VALUES(data);
UPDATE endpointman_global_vars
  SET value='10.201.2.188'
  WHERE var_name IN ('srvip', 'ntp');
UPDATE endpointman_global_vars
  SET value='america/bogota'
  WHERE var_name='tz';
UPDATE kvstore
  SET val='5060'
  WHERE module='Sipsettings' AND \`key\`='udpport-0.0.0.0';
INSERT
  INTO admin (variable, value)
  VALUES
    ('email', 'ogalaviz@kiwanoplus.com'),
    ('machineid', '$MYHOSTNAME')
  ON DUPLICATE KEY UPDATE value=VALUES(value);
INSERT
  INTO kvstore (module,\`key\`,val,type,id)
  VALUES
    ('Sipsettings','rtpend','11000',NULL,'noid'),
    ('Sipsettings','externip','$MYIP',NULL,'noid'),
    ('Sipsettings','localnets','[{"net":"10.0.0.0","mask":"8"},{"net":"172.16.0.0","mask":"12"},{"net":"192.168.0.0","mask":"16"}]','json-arr','noid'),
    ('Sipsettings','voicecodecs','{"g729":1,"gsm":2,"ulaw":3,"alaw":4}','json-arr','noid')
  ON DUPLICATE KEY UPDATE val=VALUES(val);
EOF
    fwconsole setting --import=/dev/shm/fwconsole.settings
    systemctl reload freepbx
    fwconsole notification --delete framework htaccess
    fwconsole notification --delete framework BROWSER_STATS
    fwconsole notification --delete framework missing_html5
    fwconsole notification --delete sipsettings BINDPORT
    systemctl reload freepbx
}

SetupFirewall() {
    systemctl enable firewalld
    systemctl start firewalld
    ip link | grep -Ev "link|lo:" | awk '{print $2}' | sed "s/://" | \
    while read line; do firewall-cmd --permanent --zone=internal --add-interface=$line; done
    firewall-cmd --permanent --zone=public --set-target=DROP
    firewall-cmd --permanent --zone=internal --set-target=DROP
    firewall-cmd --permanent --new-service=sip
    firewall-cmd --permanent --service=sip --add-port=5060/tcp
    firewall-cmd --permanent --service=sip --add-port=5060/udp
    firewall-cmd --permanent --service=sip --add-port=10000-11000/udp
    firewall-cmd --permanent --new-service=iax2
    firewall-cmd --permanent --service=iax2 --add-port=4569/udp
    firewall-cmd --permanent --new-service=asterisk-ami
    firewall-cmd --permanent --service=asterisk-ami --add-port=5038/tcp
    firewall-cmd --permanent --service=ssh --add-port=58790/tcp
    firewall-cmd --permanent --service=ssh --remove-port=22/tcp
    firewall-cmd --permanent --service=https --add-port=8443/tcp
    firewall-cmd --permanent --service=https --remove-port=443/tcp
    firewall-cmd --permanent --zone=internal --add-service=sip
    firewall-cmd --permanent --zone=internal --add-service=iax2
    firewall-cmd --permanent --zone=internal --add-service=https
    firewall-cmd --permanent --zone=internal --add-service=tftp
    firewall-cmd --permanent --zone=internal --add-service=ntp    
    firewall-cmd --reload
}

# Restaurar Perfil de SELinux para Asterisk
RestoreSELinux() {
    restorecon -R /etc/asterisk
    restorecon -R /var/run/asterisk
    restorecon -R /var/{lib,log,spool}/asterisk
    restorecon -R /var/lib/asterisk/agi-bin
    restorecon -R /usr/sbin/asterisk
    restorecon -R /etc/rc.d/init.d/asterisk
}

# Configurar Webmin
SetupWebmin() {
    sed -i 's/port=10000/port=9001/g' /etc/webmin/miniserv.conf
    sed -i "1s/.*/root:NDN6GDCkNpiY.:0/" /etc/webmin/miniserv.users
    systemctl enable webmin
    systemctl stop webmin
    systemctl start webmin
}

# Configurar Fail2Ban
SetupFail2Ban() {
    cat >/etc/fail2ban/filter.d/freepbx.local <<EOF
[INCLUDES]
before      = common.conf
[Definition]
_daemon     = freepbx
failregex   = Authentication failure for [^']+ from <HOST>\$
EOF
    cat >/etc/fail2ban/jail.d/customization.local <<EOF
[DEFAULT]
bantime     = 7200
maxretry    = 3
[sshd]
enabled     = true
port        = 58790
[asterisk]
enabled     = true
banaction   = iptables-multiport
[freepbx]
enabled     = true
port        = 8443
filter      = freepbx
logpath     = /var/log/asterisk/freepbx_security.log
banaction   = iptables-multiport
EOF
    cat >/etc/fail2ban/action.d/iptables-common.local <<EOF
[Init]
blocktype   = DROP
EOF
    touch /var/log/asterisk/freepbx_security.log
    chown asterisk. /var/log/asterisk/freepbx_security.log
    mkdir -p /var/run/fail2ban
    systemctl enable fail2ban
    systemctl stop fail2ban
    systemctl start fail2ban
}

SetupTFTP() {
    sed -i 's/\/var\/lib\/tftpboot/\/tftpboot/g' /etc/xinetd.d/tftp
    mkdir -p /tftpboot && chown nobody. /tftpboot && chmod 777 /tftpboot
    systemctl enable tftp
    systemctl stop tftp
    systemctl start tftp
}

SetupMisc() {
    cat >/etc/cron.d/agtech <<EOF
# Verificar y corregir direccion IP pública
*/6 * * * * asterisk /opt/agtech/updateIP
EOF
    mysql -u root asterisk <<EOF
DROP USER ''@'localhost';
DROP DATABASE test;
UPDATE mysql.user SET Password = "*ACE7311A3EFF23367D6E6E76501C9E1A96910120" WHERE User = "root";
FLUSH PRIVILEGES;
EOF
    echo 'export PS1="\[$(tput setaf 2)\]\u@\h:\w $ \[$(tput sgr0)\]"' >> /home/technicalsupport/.bash_profile
    reboot
}

SetupEnv
SetupMariaDB
InstallAsterisk
#InstallAudioFiles
InstallG729
InstallFreePBX
SetupFreePBX
SetupFirewall
RestoreSELinux
SetupWebmin
SetupFail2Ban
SetupTFTP
SetupMisc
