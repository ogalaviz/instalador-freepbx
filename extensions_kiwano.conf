; KIWANO PLUS.
; Aplicaciones personalizadas
; Version:  1.0.0
; Author:   ogalaviz@kiwanoplus.com
; Link:     http://www.kiwanoplus.com

; Algunas de las partes de este dialplan requieren la definicion de
; las siguientes variables globales en globals_custom.conf:
; KIWANO_EXTEN_LENGTH: define el numero de digitos que tienen las extensiones,
;                      colas, grupos de timbrado y similares en este IPPBX
;                      (valor por defecto: 3)



; Modificaciones a ChanSpy
; Estas modificaciones permiten realizar un espinaje dirigido:
; - 555 + Ext: Espiar
; - 556 + Ext: Susurrar
; Recuerde modificar SIP por PJSIP o IAX de acuerdo al canal utilizado
[app-chanspy-custom]
; ChanSpy - Escuchar
exten => _555X.#,1,Macro(user-callerid,)
 same =>         n,Answer
 same =>         n,NoCDR
 same =>         n,Wait(1)
 same =>         n,Playback(beep)
 same =>         n,ChanSpy(PJSIP/${EXTEN:3,q)
 same =>         n,Hangup
;ChanSpy - Susurrar
exten => _556X.#,1,Macro(user-callerid,)
 same =>         n,Answer
 same =>         n,NoCDR
 same =>         n,Wait(1)
 same =>         n,Playback(beep)
 same =>         n,ChanSpy(PJSIP/${EXTEN:3,qw)
 same =>         n,Hangup



; Modificaciones a ext-test
; Esta modificacion permite que el IPPBX haga una prueba de eco
; de los tonod DTMF, con lo cual se puede descartar que los mismos
; esten siendo reconocidos correctamente por el servidor
[ext-test-custom]
exten => *4343,1,Answer
 same =>       n,NoCDR
 same =>       n,Wait(1)
 same =>       n(start),Playback(beep)
 same =>       n,Read(digit,,1)
 same =>       n,Wait(1)
 same =>       n,SayDigits(${digit})
 same =>       n,GoTo(start)



; Modificaciones a predial-hook
; Esta modificacion almacena el UniqueID real de una llamada en una variable
; auziliar, que es heredada por todos los canales hijos, con lo cual
; QueueMetrics u otras aplicaciones pueden llevar registro del UniqueID real
; para relacionarlo con grabaciones de llamadas
[macro-dialout-trunk-predial-hook]
exten => s,1,ExecIf($["${REGEX("queuedial" ${OUT_${DIAL_TRUNK}})}" = "0"]?MacroExit)
 same =>   n,Set(__QDIALER_ORIGINALID=${UNIQUEID})



; ============================================================================
; Las siguientes líneas son el contexto usado por Queuemetrics internamente
; para sus funcionalidades. Esta basado en el archivo
; extensions_queuemetrics_18.conf que se distribuye con Queuemetrics, y ha
; sido modificado por KIWANOPLUS para satisfacer necesidades específicas
; de las implementaciones propias
; ============================================================================

[queuemetrics]
; extension 10 is a dummy end point
exten => 10,1,Answer
 same =>    n,Wait(10)
 same =>    n,Hangup

; extension 11 makes remote monitoring possible
exten => 11,1,Answer
 same =>    n,NoOp( "QM_AGENT_CODE: ${QM_AGENT_CODE}" )
 same =>    n,NoOp( "QM_EXT_MONITOR: ${QM_EXT_MONITOR}" )
 same =>    n,NoOp( "QM_AGENT_EXT: ${QM_AGENT_EXT}" )
 same =>    n,NoOp( "QM_LOGIN: ${QM_LOGIN} / ${QM_CLASS}" )
 same =>    n,NoOp( "QM_AGENT_LOGEXT: ${QM_AGENT_LOGEXT}" )
 same =>    n,ChanSpy(${QM_AGENT_LOGEXT})
 same =>    n,Hangup

; extension 12: set call status code
exten => 12,1,Answer
 same =>    n,NoOp( "QM: Setting call status '${CALLSTATUS}' on call '${CALLID}' for agent '${AGENTCODE}' made by '${QM_LOGIN}'" )
 same =>    n3,System( echo "${EPOCH}|${CALLID}|NONE|Agent/${AGENTCODE}|CALLSTATUS|${CALLSTATUS}" >> /var/log/asterisk/queue_log )
 same =>    n,Hangup

; extension 14 makes remote monitoring possible for OUTBOUND CALLS
exten => 14,1,Answer
 same =>    n,NoOp( "QM_AGENT_CODE: ${QM_AGENT_CODE}" )
 same =>    n,NoOp( "QM_EXT_MONITOR: ${QM_EXT_MONITOR}" )
 same =>    n,NoOp( "QM_AGENT_EXT: ${QM_AGENT_EXT}" )
 same =>    n,NoOp( "QM_LOGIN: ${QM_LOGIN} / ${QM_CLASS}" )
 same =>    n,ChanSpy(Local/${QM_AGENT_CODE:6}@from-internal)
 same =>    n,Hangup

; extension 16: set call status code
exten => 16,1,Answer
 same =>    n,NoOp( "QM: Setting call feature '${FEATCODE}' on call '${CALLID}' for agent '${AGENTCODE}' made by '${QM_LOGIN}' with freetext ${FEATTEXT}" )
 same =>    n,System( echo "${EPOCH}|${CALLID}|NONE|Agent/${AGENTCODE}|INFO|FTR|${FEATCODE}|${FEATTEXT}" >> /var/log/asterisk/queue_log )
 same =>    n,Hangup

; extension 17: set call status code
exten => 17,1,Answer
 same =>    n,NoOp( "QM: Removing call feature '${FEATCODE}' on call '${CALLID}' for agent '${AGENTCODE}' made by '${QM_LOGIN}' with freetext ${FEATTEXT}" )
 same =>    n,System( echo "${EPOCH}|${CALLID}|NONE|Agent/${AGENTCODE}|INFO|NOFTR|${FEATCODE}|${FEATTEXT}" >> /var/log/asterisk/queue_log )
 same =>    n,Hangup

; extension 20: agent callback login
;               For this to work, there must be no password on the agent.
exten => 20,1,Answer
 same =>    n,NoOp( "QM: Logging on Agent/${AGENTCODE} to extension ${AGENT_EXT}@from-internal made by '${QM_LOGIN}'" )
 same =>    n,AgentCallBackLogin(${AGENTCODE},,${AGENT_EXT}@from-internal)
 same =>    n,Hangup

; extension 21: agent callback logoff
exten => 21,1,Answer
 same =>    n,NoOp( "QM: Logging off Agent/${AGENTCODE} made by '${QM_LOGIN}'" )
 same =>    n,System(asterisk -rx "agent logoff Agent/${AGENTCODE}")
 same =>    n,Hangup

; extension 22: agent pause (with pause code)
exten => 22,1,Answer
 same =>    n,NoOp( "QM: Pausing Local/${AGENTCODE}@from-internal with pause reason '${PAUSEREASON}' made by '${QM_LOGIN}' " )
 same =>    n,PauseQueueMember(,Local/${AGENTCODE}@from-internal)
 same =>    n,System( echo "${EPOCH}|${UNIQUEID}|NONE|Local/${AGENTCODE}@from-internal|PAUSEREASON|${PAUSEREASON}" >> /var/log/asterisk/queue_log )
 same =>    n,Hangup

; extension 23: agent unpause
exten => 23,1,Answer
 same =>    n,NoOp( "QM: Unpausing Local/${AGENTCODE}@from-internal made by '${QM_LOGIN}' " )
 same =>    n,UnpauseQueueMember(,Local/${AGENTCODE}@from-internal)
 same =>    n,Hangup

; extension 24: agent addqueuemember (for asterisk v1.2)
exten => 24,1,Answer
 same =>    n,NoOp( "QM: AddQueueMember (asterisk v1.2) Agent/${AGENTCODE} on queue ${QUEUENAME} made by '${QM_LOGIN}'" )
 same =>    n,System( echo "${EPOCH}|${UNIQUEID}|${QUEUENAME}|Local/${AGENTCODE}@from-internal|ADDMEMBER|" >> /var/log/asterisk/queue_log )
 same =>    n,Hangup

; extension 25: agent addqueuemember (for asterisk v1.4+)
exten => 25,1,Answer
 same =>    n,NoOp( "QM: AddQueueMember (asterisk v1.4+) Agent/${AGENTCODE} on queue ${QUEUENAME} made by '${QM_LOGIN}' with prioritylabel '${QM_AGENT_PRIOLBL}' and prioritynum '${QM_AGENT_PRIONUM}'" )
 same =>    n,AddQueueMember(${QUEUENAME},Local/${AGENTCODE}@from-internal)
 same =>    n,Hangup

; extension 26: agent removequeuemember
exten => 26,1,Answer
 same =>    n,NoOp( "QM: RemoveQueueMember (asterisk v1.2) Agent/${AGENTCODE} on queue ${QUEUENAME} made by '${QM_LOGIN}'" )
 same =>    n,System( echo "${EPOCH}|${UNIQUEID}|${QUEUENAME}|Local/${AGENTCODE}@from-internal|REMOVEMEMBER|" >> /var/log/asterisk/queue_log )
 same =>    n,Hangup

; extension 27: agent removequeuemember (for asterisk v1.4+)
exten => 27,1,Answer
 same =>    n,NoOp( "QM: RemoveQueueMember (asterisk v1.4+) Agent/${AGENTCODE} on queue ${QUEUENAME} made by '${QM_LOGIN}'" )
 same =>    n,RemoveQueueMember(${QUEUENAME},Local/${AGENTCODE}@from-internal)
 same =>    n,Hangup

; extension 28: agent custom dial
exten => 28,1,Answer
 same =>    n,NoOp( "QM: Agent Custom Dial. Dialing ${EXTTODIAL} on queue ${OUTQUEUE}  made by '${QM_LOGIN}'" )
 same =>    n,Set(QDIALER_QUEUE=${OUTQUEUE})
 same =>    n,Set(QDIALER_NUMBER=${EXTTODIAL})
 same =>    n,Set(QDIALER_AGENT=Agent/${AGENTCODE})
 same =>    n,Set(QDIALER_CHANNEL=Local/${QDIALER_NUMBER}@outbound-allroutes)
 same =>    n,Set(__QDIALER_ORIGINALID=${UNIQUEID})
 same =>    n,Set(__TOUCH_MONITOR=${UNIQUEID}
 same =>    n,Set(QueueName=${QDIALER_QUEUE})
 same =>    n,Goto(qm-queuedial,s,1)
 same =>    n,Hangup

; extension 29: agent send sms (for asterisk v10+)
;exten => 29,1,NoOp( "QM: Send Text from Live Page. Sending text to ${EXTTODIAL} made by '${QM_LOGIN}' " )
; same =>    n,Set(MESSAGE(body)=From: ${QM_LOGIN} - ${MESSAGEBODY})
; same =>    n,MessageSend(sip:${EXTTODIAL})
; same =>    n,Hangup

; extension 30: soft hangup
exten => 30,1,NoOp( "QM: Call Hangup made by ${QM_LOGIN} for callID: ${CALLID} with agent code ${AGENTCODE} from extension ${QM_AGENT_LOGEXT}")
 same =>    n,ChannelRedirect(${CALLID},queuemetrics,10,3)
 same =>    n,Hangup

; extension 31: redirect
exten => 31,1,NoOp( " QM: Call redirect ,ade by ${QM_LOGIN} for callID: ${CALLID} to extension ${REDIR_EXT}")
 same =>    n,ChannelRedirect(${CALLID},from-internal,${REDIR_EXT},1)
 same =>    n,Hangup

; extension 32: agent pause with hotdesking (with pause code)
exten => 32,1,Answer
 same =>    n,NoOp( "QM: Pausing Local/${AGENTCODE}@from-internal at extension PJSIP/${QM_AGENT_LOGEXT} with pause reason '${PAUSEREASON}' made by '${QM_LOGIN}' " )
 same =>    n,PauseQueueMember(,PJSIP/${QM_AGENT_LOGEXT})
 same =>    n,System( echo "${EPOCH}|${UNIQUEID}|NONE|Local/${AGENTCODE}@from-internal|PAUSEREASON|${PAUSEREASON}" >> /var/log/asterisk/queue_log )
 same =>    n,Hangup

; extension 33: agent unpause with hotdesking
exten => 33,1,Answer
 same =>    n,NoOp( "QM: Unpausing Local/${AGENTCODE}@from-internal at extension PJSIP/${QM_AGENT_LOGEXT} made by '${QM_LOGIN}' " )
 same =>    n,UnpauseQueueMember(,PJSIP/${QM_AGENT_LOGEXT})
 same =>    n,Hangup

; extension 35: agent addqueuemember with hotdesking (for asterisk v1.4+)
exten => 35,1,Answer
 same =>    n,NoOp( "QM: AddQueueMember (asterisk v1.4+) Agent/${AGENTCODE} at extension PJSIP/${QM_AGENT_LOGEXT} on queue ${QUEUENAME} made by '${QM_LOGIN}' with prioritylabel '${QM_AGENT_PRIOLBL}' and prioritynum '${QM_AGENT_PRIONUM}'" )
 same =>    n,Macro(queuelog,${EPOCH},${UNIQUEID},NONE,Agent/${AGENTCODE},HOTDESK,PJSIP/${QM_AGENT_LOGEXT})
 same =>    n,AddQueueMember(${QUEUENAME},PJSIP/${QM_AGENT_LOGEXT},${QM_AGENT_PRIONUM})
 same =>    n,Hangup

; extension 37: agent removequeuemember with hotdesking (for asterisk v1.4+)
exten => 37,1,Answer
 same =>    n,NoOp( "QM: RemoveQueueMember (asterisk v1.4+) Agent/${AGENTCODE} at extension PJSIP/${QM_AGENT_LOGEXT} on queue ${QUEUENAME} made by '${QM_LOGIN}'" )
 same =>    n,RemoveQueueMember(${QUEUENAME},PJSIP/${QM_AGENT_LOGEXT})
 same =>    n,Hangup

;
; ================================================================
; The following dialplan is used in order to make oubound calls
; that are tracked through QueueMetrics. 
; See http://forum.queuemetrics.com/index.php?board=12.0
; ================================================================


[queuedial]
; this piece of dialplan is just a calling hook into the [qm-queuedial] context that actually does the
; outbound dialing - replace as needed - just fill in the same variables.
exten => _XXX.,1,Set(QDIALER_QUEUE=${EXTEN:0:${KIWANO_EXTEN_LENGTH}})
 same =>       n,Set(QDIALER_NUMBER=${EXTEN:${KIWANO_EXTEN_LENGTH}})
 same =>       n,Set(QDIALER_AGENT=Local/${FROMEXTEN}@from-internal)
 same =>       n,Set(QDIALER_CHANNEL=Local/${QDIALER_NUMBER}@outbound-allroutes)
 same =>       n,Set(QueueName=${QDIALER_QUEUE})
 same =>       n,Goto(qm-queuedial,s,1)


[queuedial-loggedon]
; This piece of dialplan will let only logged on agents dial out
exten => _XXX.,1,Set(QDIALER_QUEUE=${EXTEN:0:${KIWANO_EXTEN_LENGTH}})
 same =>       n,Set(QDIALER_NUMBER=${EXTEN:${KIWANO_EXTEN_LENGTH}})
 same =>       n,Set(QDIALER_AGENT=Local/${FROMEXTEN}@from-internal)
 same =>       n,Set(QDIALER_CHANNEL=Local/${QDIALER_NUMBER}@outbound-allroutes)
 same =>       n,Set(QueueName=${QDIALER_QUEUE})
 same =>       n,GotoIf($["${REGEX("Local/${FROMEXTEN}" ${QUEUE_MEMBER_LIST(${QDIALER_QUEUE})})}" = "1" ]?qm-queuedial,s,1)
 same =>       n,NoOp(Extension ${FROMEXTEN} is not logged on as agent)
 same =>       n,Wait(1)
 same =>       n,Playback(agent-loggedoff)
 same =>       n,Congestion


[queuedial-direct]
; This piece of dialplan will let only logged on agents dial out, and dial
; using queue configured in accountcode as default outbound queue
exten => _XXX.,1,ExecIf($["${CHANNEL(accountcode):0:1}" = "Q"]?Set(QDIALER_QUEUE=${CHANNEL(accountcode):1}))
 same =>       n,Set(QDIALER_NUMBER=${EXTEN})
 same =>       n,Set(QDIALER_AGENT=Local/${FROMEXTEN}@from-internal)
 same =>       n,Set(QDIALER_CHANNEL=Local/${QDIALER_NUMBER}@outbound-allroutes)
 same =>       n,Set(QueueName=${QDIALER_QUEUE})
 same =>       n,GotoIf($["${REGEX("Local/${FROMEXTEN}" ${QUEUE_MEMBER_LIST(${QDIALER_QUEUE})})}" = "1" ]?qm-queuedial,s,1)
 same =>       n,NoOp(Extension ${FROMEXTEN} is not logged on as agent)
 same =>       n,Wait(1)
 same =>       n,Playback(agent-loggedoff)
 same =>       n,Congestion


[qm-queuedial]
; We use a global variable to pass values back from the answer-detect macro.
; STATUS = U unanswered
;        = A answered    (plus CAUSECOMPLETE=C when callee hung up)
; The 'g' dial parameter must be used in order to track callee disconnecting.
; Note that we'll be using the 'h' hook in any case to do the logging when channels go down.
; We set the CDR(accountcode) for live monitoring by QM.
;
exten => s,1,NoOp( "Outbound call - A:${QDIALER_AGENT} N:${QDIALER_NUMBER} Q:${QDIALER_QUEUE} Ch:${QDIALER_CHANNEL}" )
 same =>   n,Set(CHANNEL(accountcode)=QDIALAGI)
 same =>   n,Set(ST=${EPOCH})
 same =>   n,Set(GM=QDV-${QDIALER_AGENT})
 same =>   n,Set(GLOBAL(${GM})=U)
 same =>   n,Set(GLOBAL(${GM}ans)=0)
 same =>   n,Macro(queuelog,${ST},${QDIALER_ORIGINALID},${QDIALER_QUEUE},${QDIALER_AGENT},CALLOUTBOUND,,${QDIALER_NUMBER})
 same =>   n,Dial(${QDIALER_CHANNEL},300,gM(queuedial-answer^${QDIALER_ORIGINALID}^${GM}^${QDIALER_QUEUE}^${QDIALER_AGENT}^${ST}))
 same =>   n,Set(CAUSECOMPLETE=${IF($["${DIALSTATUS}" = "ANSWER"]?C)})
; Trapping call termination here
exten => h,1,NoOp( "Call exiting: status ${GLOBAL(${GM})} answered at: ${GLOBAL(${GM}ans)} DS: ${DIALSTATUS}"  )
 same =>   n,Goto(case-${GLOBAL(${GM})})
 same =>   n,Hangup()
; Call unanswered
 same =>   n(case-U),Set(WT=$[${EPOCH} - ${ST}])
 same =>   n,Macro(queuelog,${EPOCH},${QDIALER_ORIGINALID},${QDIALER_QUEUE},${QDIALER_AGENT},ABANDON,1,1,${WT})
 same =>   n,Hangup()
; call answered: agent/callee hung
 same =>   n(case-A)i,Set(COMPLETE=${IF($["${CAUSECOMPLETE}" = "C"]?COMPLETECALLER:COMPLETEAGENT)})
 same =>   n,Set(WT=$[${GLOBAL(${GM}ans)} - ${ST}])
 same =>   n,Set(CT=$[${EPOCH} - ${GLOBAL(${GM}ans)}])
 same =>   n,Macro(queuelog,${EPOCH},${QDIALER_ORIGINALID},${QDIALER_QUEUE},${QDIALER_AGENT},${COMPLETE},${WT},${CT})
 same =>   n,Hangup()


[macro-queuedial-answer]
; Expecting $ARG1: uniqueid of the caller channel
;           $ARG2: global variable to store the answer results
;           $ARG3: queue name
;           $ARG4: agent name
;           $ARG5: enterqueue
;
exten => s,1,NoOp("Macro: queuedial-answer UID:${ARG1} GR:${ARG2} Q:${ARG3} A:${ARG4} E:${ARG5}")
 same =>   n,Set(NOW=${EPOCH})
 same =>   n,Set(WD=$[${NOW} - ${ARG5}])
 same =>   n,Macro(queuelog,${NOW},${ARG1},${ARG3},${ARG4},CONNECT,${WD})
 same =>   n,Set(GLOBAL(${ARG2})=A)
 same =>   n,Set(GLOBAL(${ARG2}ans)=${NOW})
 same =>   n,NoOp("Macro queuedial-answer terminating" )

[macro-queuelog]
; The advantage of using this macro is that you can choose whether to use the Shell version
; (where you have complete control of what gets written) or the Application version (where you
; do not need a shellout, so it's way faster).
;
; Expecting  $ARG1: Timestamp
;            $ARG2: Call-id
;            $ARG3: Queue
;            $ARG4: Agent
;            $ARG5: Verb
;            $ARG6: Param1
;            $ARG7: Param2
;            $ARG8: Param3

;exten => s,1,System( echo "${ARG1}|${ARG2}|${ARG3}|${ARG4}|${ARG5}|${ARG6}|${ARG7}|${ARG8}" >> /var/log/asterisk/queue_log )

exten => s,1,Set(ADDINFO=${ARG6}|${ARG7}|${ARG8})
 same =>   n,QueueLog(${ARG3},${ARG2},${ARG4},${ARG5},${ADDINFO})
